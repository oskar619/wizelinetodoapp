﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ToDoList.Models;
using ToDoList.Providers;

namespace ToDoList.Controllers
{
    [RoutePrefix("api/Todoes")]
    public class TodoesController : ApiController
    {
        private TodoContext db = new TodoContext();

        // GET: api/Todoes
        public IQueryable<Todo> GetTodos()
        {
            return db.Todos;
        }

        // GET: api/Todoes/5
        [ResponseType(typeof(Todo))]
        public async Task<IHttpActionResult> GetTodo(int id)
        {
            Todo todo = await db.Todos.FindAsync(id);
            if (todo == null)
            {
                return NotFound();
            }

            return Ok(todo);
        }

        // PUT: api/Todoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTodo(int id, Todo todo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != todo.Id)
            {
                return BadRequest();
            }

            db.Entry(todo).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TodoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Todoes
        [ResponseType(typeof(Todo))]
        public async Task<IHttpActionResult> PostTodo(Todo todo)
        {
            try { 
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            todo.CreationDate = DateTime.Now;
            db.Todos.Add(todo);
            await db.SaveChangesAsync();
            }catch(Exception e)
            {
                throw;
            }
            //return CreatedAtRoute("DefaultApi", new { id = todo.Id }, todo);
            return Ok(todo);  
        }

        // DELETE: api/Todoes/5
        [ResponseType(typeof(Todo))]
        public async Task<IHttpActionResult> DeleteTodo(int id)
        {
            Todo todo = await db.Todos.FindAsync(id);
            if (todo == null)
            {
                return NotFound();
            }

            db.Todos.Remove(todo);
            await db.SaveChangesAsync();

            return Ok(todo);
        }

        [Route("MarkAsDone/{id}")]
        [ResponseType(typeof(Todo))]
        [HttpGet]
        public async Task<IHttpActionResult> MarkAsDone(int id)
        {
            Todo todo = await db.Todos.FindAsync(id);
            if(todo == null)
            {
                return NotFound();
            }
            todo.Completed = true;
            db.Entry(todo).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return Ok(todo);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TodoExists(int id)
        {
            return db.Todos.Count(e => e.Id == id) > 0;
        }
    }
}