﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;
using ToDoList.Providers;

namespace ToDoList.Controllers
{
    public class TodoViewController : Controller
    {
        private ApiProvider api = new ApiProvider();
        // GET: TodoView
        public async Task<ActionResult> Index()
        {
            var todoes = await api.GetTodoes();
            return View(todoes);
        }

        // GET: TodoView/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Todo todo = await api.GetTodo(id.GetValueOrDefault(0));
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todo);
        }

        // GET: TodoView/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TodoView/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Description,Completed,CreationDate,DueDate")] Todo todo)
        {
            if (ModelState.IsValid)
            {
                await api.Create(todo);
                return RedirectToAction("Index");
            }

            return View(todo);
        }

        // GET: TodoView/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Todo todo = await api.GetTodo(id.GetValueOrDefault(0));
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todo);
        }

        // POST: TodoView/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Description,Completed,CreationDate,DueDate")] Todo todo)
        {
            if (ModelState.IsValid)
            {
                await api.Update(todo);
                return RedirectToAction("Index");
            }
            return View(todo);
        }

        // GET: TodoView/5
        [Route("Done/{id}")]
        public async Task<ActionResult> Done(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Todo todo = await api.MarkAsDone(id.GetValueOrDefault(0));
            if (todo == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                api.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
