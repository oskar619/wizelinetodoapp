﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ToDoList.Models;

namespace ToDoList.Providers
{
    public class TodoContext : DbContext
    {

        public TodoContext() : base("DefaultConnection")
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Todo>().ToTable(Tables.Todo);
            modelBuilder.Entity<Todo>().HasKey<int>(x => x.Id);
            modelBuilder.Entity<Todo>().Property(x => x.Id).HasColumnName(Columns.Id);
            modelBuilder.Entity<Todo>().Property(x => x.Description).HasColumnName(Columns.Description);
            modelBuilder.Entity<Todo>().Property(x => x.Completed).HasColumnName(Columns.Completed);
            modelBuilder.Entity<Todo>().Property(x => x.CreationDate).HasColumnName(Columns.CreationDate);
            modelBuilder.Entity<Todo>().Property(x => x.DueDate).HasColumnName(Columns.DueDate);

        }


        private static class Tables
        {
            public const string Todo = "todoitem";
        }

        private static class Columns
        {
            public const string Id = "id";
            public const string Description = "description";
            public const string Completed = "completed";
            public const string CreationDate = "creationdate";
            public const string DueDate = "duedate";
        }

        public DbSet<Todo> Todos { get; set; }
    }

    
}