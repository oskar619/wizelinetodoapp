﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ToDoList.Models;

namespace ToDoList.Providers
{
    public class ApiProvider : IDisposable
    {
        private const string baseUrl = "http://localhost:53944/api/{0}";

        private const string Todoes = "Todoes";
        private const string GetTodoesStr = "Todoes/{0}";
        private const string MarkAsDoneStr = "Todoes/MarkAsdone/{0}";

        public ApiProvider()
        {

        }

        public async Task<Todo> GetTodo(int id)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format(baseUrl, string.Format(GetTodoesStr,id)));
                var result = await request.GetResponseAsync();
                var stream = result.GetResponseStream();
                StreamReader objReader = new StreamReader(stream);
                var sbuilder = new StringBuilder();
                string sLine = "";
                int i = 0;

                while (sLine != null)
                {
                    i++;
                    sLine = objReader.ReadLine();
                    sbuilder.Append(sLine);
                    if (sLine != null)
                        Console.WriteLine("{0}:{1}", i, sLine);
                }
                var todo = JsonConvert.DeserializeObject<Todo>(sbuilder.ToString());
                return todo;
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public async Task<List<Todo>> GetTodoes()
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format(baseUrl, Todoes));
                var result = await request.GetResponseAsync();
                var stream = result.GetResponseStream();
                StreamReader objReader = new StreamReader(stream);
                var sbuilder = new StringBuilder();
                string sLine = "";
                int i = 0;

                while (sLine != null)
                {
                    i++;
                    sLine = objReader.ReadLine();
                    sbuilder.Append(sLine);
                    if (sLine != null)
                        Console.WriteLine("{0}:{1}", i, sLine);
                }
                var todoList = JsonConvert.DeserializeObject<Todo[]>(sbuilder.ToString());
                return todoList.ToList();

            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public async Task<Todo> MarkAsDone(int id)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format(baseUrl, string.Format(MarkAsDoneStr,id)));
                var result = await request.GetResponseAsync();
                var stream = result.GetResponseStream();
                StreamReader objReader = new StreamReader(stream);
                var sbuilder = new StringBuilder();
                string sLine = "";
                int i = 0;

                while (sLine != null)
                {
                    i++;
                    sLine = objReader.ReadLine();
                    sbuilder.Append(sLine);
                    if (sLine != null)
                        Console.WriteLine("{0}:{1}", i, sLine);
                }
                var todo = JsonConvert.DeserializeObject<Todo>(sbuilder.ToString());
                return todo;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<Todo> Update(Todo todo)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format(baseUrl, string.Format(GetTodoesStr,todo.Id)));
                ASCIIEncoding encoding = new ASCIIEncoding();
                string stringData = JsonConvert.SerializeObject(todo); //place body here
                byte[] data = encoding.GetBytes(stringData);
                request.Method = "PUT";
                request.ContentType = "application/json"; //place MIME type here
                request.ContentLength = data.Length;

                Stream newStream = request.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();

                var result = await request.GetResponseAsync();
                var stream = result.GetResponseStream();
                StreamReader objReader = new StreamReader(stream);
                var sbuilder = new StringBuilder();
                string sLine = "";
                int i = 0;

                while (sLine != null)
                {
                    i++;
                    sLine = objReader.ReadLine();
                    sbuilder.Append(sLine);
                    if (sLine != null)
                        Console.WriteLine("{0}:{1}", i, sLine);
                }
                var ntodo = JsonConvert.DeserializeObject<Todo>(sbuilder.ToString());
                return ntodo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<Todo> Create(Todo todo)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format(baseUrl, string.Format(GetTodoesStr, todo.Id)));
                ASCIIEncoding encoding = new ASCIIEncoding();
                string stringData = JsonConvert.SerializeObject(todo); //place body here
                byte[] data = encoding.GetBytes(stringData);
                request.Method = "POST";
                request.ContentType = "application/json"; //place MIME type here
                request.ContentLength = data.Length;

                Stream newStream = request.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();

                var result = await request.GetResponseAsync();
                var stream = result.GetResponseStream();
                StreamReader objReader = new StreamReader(stream);
                var sbuilder = new StringBuilder();
                string sLine = "";
                int i = 0;

                while (sLine != null)
                {
                    i++;
                    sLine = objReader.ReadLine();
                    sbuilder.Append(sLine);
                    if (sLine != null)
                        Console.WriteLine("{0}:{1}", i, sLine);
                }
                var ntodo = JsonConvert.DeserializeObject<Todo>(sbuilder.ToString());
                return ntodo;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public void Dispose()
        {
            
        }
    }
}